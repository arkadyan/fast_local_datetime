defmodule FastLocalDatetime.Refresher do
  @moduledoc """
  Server which makes sure the tables are kept up to date.

  When it detects an update to the timezone database, it calls
  `TableServer.refresh/1` for each running table.
  """
  use GenServer
  require Logger
  alias FastLocalDatetime.{TableSupervisor, TableServer}

  def start_link([]) do
    GenServer.start_link(__MODULE__, [])
  end

  @impl GenServer
  def init([]) do
    current_version = Tzdata.tzdata_version()
    :ok = schedule_refresh()
    {:ok, current_version}
  end

  @impl GenServer
  def handle_info(:check_refresh, last_version) do
    current_version = Tzdata.tzdata_version()

    unless current_version == last_version do
      Logger.debug(fn ->
        "fast_local_datetime: updating from #{last_version} to #{current_version}"
      end)

      :ok = refresh()
    end

    :ok = schedule_refresh()
    {:noreply, current_version}
  end

  def schedule_refresh do
    Process.send_after(self(), :check_refresh, refresh_interval())
    :ok
  end

  def refresh do
    for pid <- TableSupervisor.children() do
      :ok = TableServer.refresh(pid)
    end

    :ok
  end

  def refresh_interval() do
    Application.get_env(:fast_local_datetime, :refresh_check_interval)
  end
end
