defmodule FastLocalDatetime.Table do
  @moduledoc """
  Maintains a table of timezone periods for a given timezone, and allows
  those timezones to convert a UTC epoch timestamp into a DateTime.
  """

  @opaque t :: %__MODULE__{}

  defstruct [:name, :table]

  @epoch {{1970, 1, 1}, {0, 0, 0}}
  @gregorian_offset :calendar.datetime_to_gregorian_seconds(@epoch)

  @doc """
  Creates a new table for epoch conversions.

  Returns `{:ok, table}` on success, or `{:error, :not_found}` if the
  timezone doesn't exist.

      iex> {:ok, table} = new("America/New_York")
      iex> table
      #FastLocalDatetime.Table<America/New_York>

      iex> new("not found")
      {:error, :not_found}
  """
  @spec new(String.t()) :: {:ok, t} | {:error, :not_found}
  def new(timezone) when is_binary(timezone) do
    with {:ok, periods} <- Tzdata.periods(timezone) do
      table = :ets.new(__MODULE__, [:duplicate_bag, {:read_concurrency, true}])
      true = :ets.insert(table, table_data(periods))
      {:ok, %__MODULE__{name: timezone, table: table}}
    end
  end

  @doc """
  Returns the timezone of the table.

      iex> {:ok, table} = new("America/New_York")
      iex> timezone(table)
      "America/New_York"
  """
  def timezone(%__MODULE__{name: name}), do: name

  @doc """
  Deletes a previously created table.

      iex> {:ok, table} = new("America/New_York")
      iex> delete(table)
      :ok
  """
  @spec delete(t) :: :ok
  def delete(%__MODULE__{table: table}) do
    true = :ets.delete(table)
    :ok
  end

  @doc """
  Returns true if the ETS table backing the table is still present.

      iex> {:ok, table} = new("America/New_York")
      iex> valid?(table)
      true
      iex> delete(table)
      :ok
      iex> valid?(table)
      false
  """
  def valid?(%__MODULE__{table: table}) do
    :ets.info(table, :type) == :duplicate_bag
  end

  @doc """
  Convert a given UTC epoch timestamp to a local datetime.

      iex> {:ok, table} = new("America/New_York")
      iex> unix_to_datetime(table, 1522888537)
      #DateTime<2018-04-04 20:35:37-04:00 EDT America/New_York>
      iex> unix_to_datetime(table, -218937198213123)
      {:error, :before_year_zero}

      iex> {:ok, table} = new("Etc/GMT-5")
      iex> unix_to_datetime(table, 1522888537)
      #DateTime<2018-04-05 05:35:37+05:00 +05 Etc/GMT-5>
  """
  @spec unix_to_datetime(t, integer) :: DateTime.t()
  def unix_to_datetime(%__MODULE__{name: zone_name, table: table}, unix)
      when unix > -@gregorian_offset do
    {utc_off, std_off, zone_abbr} = unix_to_timezone(table, unix)

    {{year, month, day}, {hour, minute, second}} =
      :calendar.gregorian_seconds_to_datetime(@gregorian_offset + unix + utc_off + std_off)

    %DateTime{
      calendar: Calendar.ISO,
      year: year,
      month: month,
      day: day,
      hour: hour,
      minute: minute,
      second: second,
      time_zone: zone_name,
      zone_abbr: zone_abbr,
      utc_offset: utc_off,
      std_offset: std_off
    }
  end

  def unix_to_datetime(%__MODULE__{}, unix) when is_integer(unix) do
    {:error, :before_year_zero}
  end

  defp table_data(periods) do
    for %{
          from: %{utc: from},
          until: %{utc: until},
          std_off: std_off,
          utc_off: utc_off,
          zone_abbr: zone_abbr
        } <- periods do
      from = if from == :min, do: 0, else: from - @gregorian_offset
      until = if until == :max, do: :max, else: until - @gregorian_offset
      {from, until, {utc_off, std_off, zone_abbr}}
    end
  end

  defp unix_to_timezone(tab, unix) do
    {[timezone], _} =
      :ets.select(
        tab,
        [
          {
            {:"$1", :"$2", :"$3"},
            [
              {:>=, unix, :"$1"},
              {:>, :"$2", unix}
            ],
            [:"$3"]
          }
        ],
        1
      )

    timezone
  end

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(%{name: name}, _opts) do
      concat(["#FastLocalDatetime.Table<", name, ">"])
    end
  end
end
