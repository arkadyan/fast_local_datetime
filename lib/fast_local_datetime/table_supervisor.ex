defmodule FastLocalDatetime.TableSupervisor do
  @moduledoc """
  :simple_one_for_one supervisor to manage the TableServer children.
  """
  use Supervisor

  def start_link([]) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def start_child(timezone) do
    Supervisor.start_child(__MODULE__, [timezone])
  end

  def children do
    for {_, pid, _, _} <- Supervisor.which_children(__MODULE__), is_pid(pid) do
      pid
    end
  end

  @impl Supervisor
  def init([]) do
    children = [
      %{
        id: :table_server,
        start: {FastLocalDatetime.TableServer, :start_link, []}
      }
    ]

    Supervisor.init(children, strategy: :simple_one_for_one)
  end
end
