# Changelog

## 0.2.0

- Fix a bug where timestamps before the year 0 would crash with a MatchError

## 0.1.0

- Initial release
