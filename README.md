# FastLocalDatetime

[![pipeline status](https://gitlab.com/paulswartz/fast_local_datetime/badges/master/pipeline.svg)](https://gitlab.com/paulswartz/fast_local_datetime/commits/master) [![coverage report](https://gitlab.com/paulswartz/fast_local_datetime/badges/master/coverage.svg)](https://gitlab.com/paulswartz/fast_local_datetime/commits/master)

Fast conversion of a UTC epoch timestamp (Unix timestamp) into a DateTime in
a given timezone.

## Examples

```elixir
iex> {:ok, dt} = unix_to_datetime(1522888537, "America/New_York")
iex> dt
#DateTime<2018-04-04 20:35:37-04:00 EDT America/New_York>

iex> unix_to_datetime(1522888537, "not found")
{:error, :not_found}
```

## Benchmarks

```
$ mix run bench/unix.exs

Operating System: macOS
CPU Information: Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz
Number of Available Cores: 8
Available memory: 16 GB
Elixir 1.6.4
Erlang 20.3
Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
parallel: 1
inputs: none specified
Estimated total run time: 35 s


Benchmarking calendar...
Benchmarking calendar_only...
Benchmarking fast_local_datetime...
Benchmarking timex...
Benchmarking timex_only...

Name                          ips        average  deviation         median         99th %
fast_local_datetime       79.97 K       12.50 μs   ±174.96%          11 μs          31 μs
calendar_only             19.47 K       51.35 μs    ±33.97%          44 μs      117.44 μs
calendar                  18.46 K       54.17 μs    ±28.64%          47 μs         119 μs
timex_only                 6.47 K      154.55 μs    ±29.99%         140 μs         358 μs
timex                      6.33 K      157.93 μs    ±25.77%         145 μs      305.14 μs

Comparison:
fast_local_datetime       79.97 K
calendar_only             19.47 K - 4.11x slower
calendar                  18.46 K - 4.33x slower
timex_only                 6.47 K - 12.36x slower
timex                      6.33 K - 12.63x slower
```

The `*_only` benchmarks don't include the conversion from a Unix timestamp to
a DateTime: they only test the timezone conversion of the given library.

## Installation

The package can be installed by adding `fast_local_datetime` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:fast_local_datetime, "~> 0.1.0"}
  ]
end
```

Documentation can be found at [https://hexdocs.pm/fast_local_datetime](https://hexdocs.pm/fast_local_datetime).

## Updates

`fast_local_datetime` keeps the tables up-to-date with `tzdata` by checking
periodically whether a new database has been loaded. By default, they're
checked every hour. This can be overriden in the configuration:

```elixir
# refresh once per day

config :fast_local_datetime, refresh_check_interval: 86_400_00
```
