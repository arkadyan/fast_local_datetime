defmodule FastLocalDatetime.TableServerTest do
  @moduledoc false
  use ExUnit.Case
  import FastLocalDatetime.TableServer
  alias FastLocalDatetime.{Table, TableRegistry}
  @timezone "America/Los_Angeles"
  setup do
    {:ok, pid} = start_link(@timezone)
    {:ok, %{pid: pid}}
  end

  describe "start_link/1" do
    test "registers a table", %{pid: pid} do
      assert [{^pid, %Table{} = table}] = Registry.lookup(TableRegistry, @timezone)
      assert Table.timezone(table) == @timezone
    end
  end

  describe "refresh/1" do
    test "replaces the registered table with a new one", %{pid: pid} do
      [{^pid, old_table}] = Registry.lookup(TableRegistry, @timezone)
      refresh(pid)
      [{^pid, new_table}] = Registry.lookup(TableRegistry, @timezone)
      refute new_table == old_table
      refute Table.valid?(old_table)
      assert Table.valid?(new_table)
    end
  end
end
